import {Actor, Task, Question, createTask, createQuestion} from 'cypress-screenplay';
import LoginPage from '../PageObjects/LoginPage';

const loginAsAdmin = createTask(() => {
    const lp = new LoginPage();
    lp.visit();
    lp.fillLogin("admin");
    lp.fillPassword("hoanghoang");
    lp.submitLogin();
  });

const loginAsUser = createTask(() => {
    const lp = new LoginPage();
    lp.visit();
    lp.fillLogin("notadmin");
    lp.fillPassword("hoanghoang");
    lp.submitLogin();
})

const readMenuItems = createQuestion((cy, param, assert) => {
    cy.get("#top-menu>ul>li").should((items) =>
        assert(
            items
              .toArray()
              .map((item) => item.textContent)
              .filter((item) => item !== null)
          )
    );
})

describe('Should see admin rights', () => {
    const admin = new Actor();
    const user = new Actor();

    it('lets admin see administration', () => {
        admin
        .perform(loginAsAdmin)
        .ask(readMenuItems, undefined, (items) => {
            expect(items).to.contain('Administration')
        })
    });

    it('does not let a normal user see administration', () => {
        user
        .perform(loginAsUser)
        .ask(readMenuItems, undefined, (items) => {
            expect(items).not.to.contain('Administration');
        })
    })
})