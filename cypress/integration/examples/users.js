import LoginPage from "./PageObjects/LoginPage";
import UserAdministrationPage from "./PageObjects/UserAdministrationPage";
/// <reference types="cypress" />
describe('Sign In', () => {
    beforeEach(() => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
    })
    it('Create a user', () => {
        const userP = new UserAdministrationPage();
        userP.visit();
        userP.addUser();
        userP.addLogin("testUser1")
        userP.addFName("First1");
        userP.addLName("Last1");
        userP.addEmail("test@test.cz");
        userP.addPassword("password123");
        userP.addConfirm("password123");
        userP.createUser();
        cy.get(".flash").should('have.text', 'User testUser1 created.')
    })
});
after(() => {
    cy.visit("http://147.32.83.191:8080/users");
    cy.get("td.username").contains("testUser1").closest("tr.user").find("a.icon-del").click();
    cy.on('window:confirm', () => true);
})