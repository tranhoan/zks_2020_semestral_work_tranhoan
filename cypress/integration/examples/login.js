import LoginPage from "./PageObjects/LoginPage";
import UserAdministrationPage from "./PageObjects/UserAdministrationPage";
/// <reference types="cypress" />
describe('Sign In', () => {
    it('Let you log in', () => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
        cy.get("#account>ul>li:last-child>a").should('have.text','Sign out')
    });

    it('Empty inputs', () => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin(" ");
        lp.fillPassword(" ");
        lp.submitLogin();
        cy.get(".flash").should('have.text', 'Invalid user or password')

        
    })
});