import LoginPage from "../PageObjects/LoginPage";

describe('Request stubbing', () => {
    it('Fake projects', () => {
        cy.intercept('GET', 'http://147.32.83.191:8080/admin/projects', { fixture: 'projects.html' }).as('getProjects')
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
        cy.visit("http://147.32.83.191:8080/admin/projects")
        cy.wait(['@getProjects']).then((interception) => {
            assert.isNotNull(interception.response.body, '1st API call has data')
        })

        cy.get('table.list tbody tr')
        .should(($row) => {
            expect($row).to.have.length(4);
        })
    })
})