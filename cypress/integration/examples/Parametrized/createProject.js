import { UseCypress } from "cypress-screenplay"
import LoginPage from "../PageObjects/LoginPage";

describe('Create project by stubbing', () => {
    beforeEach(() => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
    })
    it('Fake user', () => {
        cy.visit("http://147.32.83.191:8080/projects/new");
        cy.fixture('outputProject.json').then((p) => {
           p.forEach((project, index) => {
               if(project.Name!="NULL") {
                   cy.get("#project_name").clear().type(project.Name+index);
               }
               cy.get("#project_description").clear().type(project.Description);
               if(project.Identifier!="NULL") {
                   cy.get("#project_identifier").clear().type(project.Identifier+index);
               }
               else {
                   cy.get("#project_identifier").clear()
               }
               cy.get("#project_homepage").clear().type(project.Homepage);
               if(project.Public=="false"){
                   cy.get("#project_is_public").uncheck();
               }
               if(project.Inherit=="true"){
                   cy.get("#project_inherit_members").check();
               }
               cy.get("input[value='Create']").click();
               let messageId = project.Result.split(" ")[0]=="Successful" ? "#flash_notice" : "#errorExplanation>ul>li:first-child";
               cy.get(messageId).should('have.text',project.Result);
               cy.visit("http://147.32.83.191:8080/projects/new")
           });
        })
    })
})
after(() => {
    cy.visit("http://147.32.83.191:8080/admin/projects");
        for(let i=0;i<4;i++) {
            cy.get("tr.project td.name a").filter("[title='valid']").first().closest("tr.project").find("a.icon-del").click();
            cy.get("#confirm").check();
            cy.get("input[value='Delete']").click();
        }
})