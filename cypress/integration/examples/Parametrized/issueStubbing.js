import LoginPage from "../PageObjects/LoginPage";

describe('Request stubbing', () => {
    beforeEach(() => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
    })
    it('Fake issues', () => {
        cy.intercept('GET', 'http://147.32.83.191:8080/issues', { fixture: 'issues.html' }).as('getIssues')
        cy.get("#top-menu .projects").click();
        cy.get(".issues").click();
        cy.wait(['@getIssues']).then((interception) => {
            assert.isNotNull(interception.response.body, '1st API call has data')
        })

        cy.get('table.issues tbody tr')
        .should(($row) => {
            expect($row).to.have.length(6)
        })
    })
})