import { UseCypress } from "cypress-screenplay"
import LoginPage from "../PageObjects/LoginPage";

describe('Create user by stubbing', () => {
    beforeEach(() => {
        const lp = new LoginPage();
        lp.visit();
        lp.fillLogin("admin");
        lp.fillPassword("hoanghoang");
        lp.submitLogin();
    })
    it('Fake user', () => {
        cy.visit("http://147.32.83.191:8080/users");
        cy.get(".icon-add").click()
        cy.fixture('outputUser.json').then((users) => {
           users.forEach((user, index) => {
               if(user.User!="NULL") {
                   cy.get("#user_login").clear().type(user.User);
               }
               cy.get("#user_password").clear().type(user.Password);
               if(user.Fname!="NULL") {
                cy.get("#user_firstname").clear().type(user.Fname);
               }
               if(user.Lname!="NULL") {
                cy.get("#user_lastname").clear().type(user.Lname);
               }
               if(user.Email!="NULL") {
                   cy.get("#user_mail").clear().type(index+user.Email);
               }
               if(user.Confirmation!="NULL") {
                    cy.get("#user_password_confirmation").clear().type(user.Confirmation)
               }
               cy.get("input[value='Create']").click();
               console.log(user.Result.split(" ")[0]=="User");
               let messageId = user.Result.split(" ")[0]=="User" ? "#flash_notice" : "#errorExplanation>ul>li:first-child";
               console.log(messageId)
               cy.get(messageId).should('have.text',user.Result);
               cy.visit("http://147.32.83.191:8080/users/new")
           });
        })
    })
})
after(() => {
    cy.visit("http://147.32.83.191:8080/users");
    cy.get("td.username").contains("valid_username7").closest("tr.user").find("a.icon-del").click();
    cy.on('window:confirm', () => true);
})