class UserAdministrationPage {
    
    visit(){
        cy.visit("http://147.32.83.191:8080/users")
    }

    addUser() {
        const addButton = cy.get("a.icon-add");
        addButton.click();
        return this;
    }

    addLogin(user) {
        const input = cy.get("#user_login");
        input.type(user);
        return this;
    }

    addFName(fname) {
        const fInput = cy.get("#user_firstname");
        fInput.type(fname);
        return this;
    }

    addLName(lname) {
        const lInput = cy.get("#user_lastname");
        lInput.type(lname);
        return this;
    }

    addEmail(email) {
        const eInput = cy.get("#user_mail");
        eInput.type(email);
        return this;
    }

    addPassword(password) {
        const pInput = cy.get("#user_password");
        pInput.type(password);
        return this;
    }

    addConfirm(confirm) {
        const cInput = cy.get("#user_password_confirmation");
        cInput.type(confirm);
        return this;
    }

    createUser() {
        const createButton = cy.get('input[value="Create"]')
        createButton.click();
        return this;
    }


    
}

export default UserAdministrationPage;