class LoginPage {
    
    visit(){
        cy.visit("http://147.32.83.191:8080/login")
    }

    fillLogin(username) {
        const loginInput = cy.get("#username");
        loginInput.clear();
        loginInput.type(username);
        return this;
    }

    fillPassword(password) {
        const pwInput = cy.get("#password");
        pwInput.clear();
        pwInput.type(password);
        return this;
    }
    
    submitLogin() {
        const button = cy.get("#login-submit");
        button.click()
        return this;
    }
}

export default LoginPage;